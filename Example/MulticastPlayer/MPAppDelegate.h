//
//  MPAppDelegate.h
//  MulticastPlayer
//
//  Created by zmt on 10/14/2020.
//  Copyright (c) 2020 zmt. All rights reserved.
//

@import UIKit;

@interface MPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
