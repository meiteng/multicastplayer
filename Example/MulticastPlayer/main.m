//
//  main.m
//  MulticastPlayer
//
//  Created by zmt on 10/14/2020.
//  Copyright (c) 2020 zmt. All rights reserved.
//

@import UIKit;
#import "MPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MPAppDelegate class]));
    }
}
