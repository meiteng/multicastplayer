# MulticastPlayer

[![CI Status](https://img.shields.io/travis/zmt/MulticastPlayer.svg?style=flat)](https://travis-ci.org/zmt/MulticastPlayer)
[![Version](https://img.shields.io/cocoapods/v/MulticastPlayer.svg?style=flat)](https://cocoapods.org/pods/MulticastPlayer)
[![License](https://img.shields.io/cocoapods/l/MulticastPlayer.svg?style=flat)](https://cocoapods.org/pods/MulticastPlayer)
[![Platform](https://img.shields.io/cocoapods/p/MulticastPlayer.svg?style=flat)](https://cocoapods.org/pods/MulticastPlayer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MulticastPlayer is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MulticastPlayer'
```

## Author

zmt, 1277097685@qq.com

## License

MulticastPlayer is available under the MIT license. See the LICENSE file for more info.
